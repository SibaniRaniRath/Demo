//
//  AppConstant.swift
//  Demo
//
//  Created by IT/LP/22 on 07/06/23.
//

import Foundation
import UIKit

class AppConstant: NSObject, UIAlertViewDelegate {
    static var screenSize = UIScreen.main.bounds
    class func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
                return ["parseError":error.localizedDescription]
            }
        }
        return nil
    }
    
    class func convertToArray(text: String) -> [[String: Any]]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
            } catch {
                print(error.localizedDescription)
                return [["parseError":error.localizedDescription]]
            }
        }
        return nil
    }
    
}
extension UIViewController {
    func showAlert(alertTitle: String, delegate: AnyObject?) {
        let alert = UIAlertController(title: alertTitle, message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        alert.view.tintColor = UIColor.blue
        self.present(alert, animated: true, completion: nil)
    }
}

