//
//  ListDataTableViewCell.swift
//  Demo
//
//  Created by IT/LP/22 on 07/06/23.
//

import UIKit

class ListDataTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnCheckbox: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //viewContent.layer.cornerRadius = 10
        //viewContent.layer.borderWidth = 1
        //viewContent.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
