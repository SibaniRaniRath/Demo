//
//  ViewController.swift
//  Demo
//
//  Created by IT/LP/22 on 07/06/23.
//

import UIKit
import Alamofire
import SDWebImage

struct MyObj: Decodable {
    let id: String
    let author: String
    let width: Int
    let height: Int
    let url: String
    let download_url: String
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    
    var arrList = [MyObj]()
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initDesign()
    }
    
    func initDesign() {
        
        tblView.delegate = self
        tblView.dataSource = self
        tblView.tableFooterView = UIView()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblView.addSubview(refreshControl)
        
        self.apiCallToGetListData()
    }
    
    //MARK: Tableview Delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "listData", for: indexPath ) as! ListDataTableViewCell
        cell.selectionStyle = .none
        
        let dataBo = arrList[indexPath.row]
        cell.lblTitle.text = dataBo.id
        cell.lblDesc.text = dataBo.author
        
        URLSession.shared.dataTask(with: URL(string:dataBo.download_url)!){(data, response, error) in
        if let error = error {
            print("Error: \(error)")
        }else if let data = data {
            DispatchQueue.main.async{
                cell.imgView.image = UIImage(data: data)
            }
        }
        
        }.resume ()

        cell.btnCheckbox.tag = indexPath.row
        
        return cell
        
    }
    
    //MARK: Button Actions
    @IBAction func btnCheckBoxClicked(_ sender: UIButton){
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = tblView.cellForRow(at: indexPath) as! ListDataTableViewCell
        cell.btnCheckbox.isSelected = !cell.btnCheckbox.isSelected
    }
    
    @objc func refresh(_ sender: Any) {
        tblView.reloadData()
        refreshControl.endRefreshing()
    }
    
    //MARK: ServiceCall Methods
    func apiCallToGetListData() {
        var url = "https://picsum.photos/v2/list?page=2&limit=20"
        url = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        print("url===\(url)")
        //let headers: HTTPHeaders = ["Accept": "application/json"]
        //print("Header=== \(headers)")
        
        AF.request( url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).responseData{response in
            debugPrint(response)
            switch(response.result){
                case let .success(value):
                
                do {
                    self.arrList = try JSONDecoder().decode([MyObj].self, from: value)
                    
                    self.tblView.reloadData()
                    
                    debugPrint("Arr: \(self.arrList)")
                } catch let decodeError {
                    debugPrint("Failed: ", decodeError)
                }
                break
                case let .failure(error):
                self.showAlert(alertTitle: error.localizedDescription, delegate: self)
                break
                
            }
            
        }
        
        }
    
}

